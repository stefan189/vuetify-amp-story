import SliderCounter from "./SliderCounter"

export default {
    name:'slider',
    components:{SliderCounter},
    data(){
        return{
            titleEditor: false,
            descriptionEditor: false,
        }
    },
    computed:{
        pages(){
            return this.$store.getters['story/pages'];
        },
        slider(){
            return this.$store.getters['story/slider'];
        },
        selectedPage(){
            return this.$store.getters['story/selectedPage'];
        },
        prevPage(){
            return this.$store.getters['story/prevPage'];
        },
        nextPage(){
            return this.$store.getters['story/nextPage'];
        },
        tools(){
            return this.$store.getters['tools/selectedTools'];
        },
    },
    methods:{
        disableEditor(){
            //disables text-area on screen for text edit
            this.titleEditor ?this.$set(this, "titleEditor", false) :false;
            this.descriptionEditor ?this.$set(this, "descriptionEditor", false) :false;
        },
        enableEditor(type){
            //enables text-area on screen to edit text
            this.$set(this, `${type}`, true);
        },
        mouseDown(e, dragedItem){
            //instance variables
            let self = this;
            let target = e.target;
            let targetOffsetX = parseInt(target.style.left);
			let targetOffsetY = parseInt(target.style.top);
            let targetHeight = parseInt(target.offsetHeight);
            let targetWidth = parseInt(target.offsetWidth);
            let mouseX = e.clientX;
			let mouseY = e.clientY;
            let canvasHeight = document.querySelector('.center').offsetHeight;
            let canvasWidth = document.querySelector('.center').offsetWidth;

            const { addEventListener, removeEventListener } = window;

            //if text settings icon is not selecetd auto switch to it
            if(self.tools.name != 'text') self.$store.commit( 'tools/updateSelectedTool', {selectedTabName:"story", index:3} )

            //change selectedItem on canvas if not selected already
            if(dragedItem != this.slider.selectedItem) self.$store.commit('story/updateSelectedItem', dragedItem) ;

            //function definitions
            function onMouseMove(e){
                if(e.clientY-mouseY != 0 || e.clientX-mouseX != 0){
                    let xVal = targetOffsetX+e.clientX-mouseX,
                        yVal = targetOffsetY+e.clientY-mouseY,
                        newOffset = constrainToEdges(xVal, yVal);

                    self.$store.commit('story/updateCSS', { type:dragedItem, newValues:{x: newOffset.x, y: newOffset.y} });
                }
            }
            function onMouseUp(){
                removeEventListener('mousemove', onMouseMove);
                removeEventListener('mouseup', onMouseUp);
            }

            function constrainToEdges(cordX, cordY){
                let x = 0, y = 0;

                //if not hitting left wall
                if(cordX > 0 ) (targetWidth + cordX <= canvasWidth ) ?x = cordX :x = (canvasWidth - targetWidth-1);
                //if not hitting top wall
                if(cordY > 0 ) (targetHeight + cordY <= canvasHeight ) ?y = cordY :y = (canvasHeight - targetHeight);

                return {x, y}
            }

            //add event listeners
            addEventListener('mousemove', onMouseMove);
            addEventListener('mouseup', onMouseUp);
         }
    }
}
