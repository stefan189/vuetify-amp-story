import Vue from 'vue'
import '@/plugins/vuetify'
import Vuetify from 'vuetify'
import App from '@/App.vue'
import * as firebase from "firebase"
import router from '@/router'
import store from '@/store/store'
import DefaultLayout from "@/components/layouts/DefaultLayout"

/**************************************************************************************************
 *  Global components
 **************************************************************************************************/
Vue.component('DefaultLayout', DefaultLayout)

Vue.use(Vuetify, {
  theme: {
    primary: '#3f51b5',
    secondary: '#b0bec5',
    accent: '#8c9eff',
    error: '#b71c1c',
    white: '#ffffff',
    pearl: '#f8f8f8',
    black: '#000000',
    bunker: '#0C0F17',
    mirage: '#151a26',
    mirageLight: '#1b212f',
    royalBlue: '#5628ee',
    ebonyClay: '#2F3545',
    gray: '#626a7b',
    fb: '#4366b0',
    gp: '#da483e',
    twt: '#2394de'
  }
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  created(){
      firebase.initializeApp({
          apiKey: "AIzaSyBC88Ih4H-DEe6M6Plf7glMTsV0d36ImsI",
          authDomain: "amp-vuetify.firebaseapp.com",
          databaseURL: "https://amp-vuetify.firebaseio.com",
          projectId: "amp-vuetify",
          storageBucket: "amp-vuetify.appspot.com",
      })
  }
}).$mount('#app')
