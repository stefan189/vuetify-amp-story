const ENDPOINTS = {
    root:"https://amp-vuetify.firebaseio.com/",
}

export default class API{
    constructor(){
        this.urls = ENDPOINTS;
    }

    get(url){
        return fetch(`${this.urls.root}${url}`)
        .then(response => response.json())
    }

    post(url, data = {}) {
        // Default options are marked with *
        return fetch(url, {
            method: "POST",
            mode: "cors",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data), // body data type must match "Content-Type" header
        })
    }
}
