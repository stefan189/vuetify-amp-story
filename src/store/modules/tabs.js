// import Vue from 'vue'
/*************************************************************************************************
 *                                              STATE
 *************************************************************************************************/
const state = {
    tabs:[
        {id:1, name:'story', selected:true, route:null },
        {id:2, name:'report', selected:false, route:null},
    ],
}

/*************************************************************************************************
 *                                             GETTERS
 *************************************************************************************************/
const getters = {
    tabs(state){
        return state.tabs;
    },
    selectedTab(state){
        //get selected Tab Obj wiht index prop
        return state.tabs.filter((item, i)=>{
            if(item.selected==true){
                item.index = i;
                return item;
            }

        })[0];
    }
}

/*************************************************************************************************
 *                                           MUTATIONS
 *************************************************************************************************/
const mutations = {
    updateSelectedTab(state, payload){
        state.tabs.map((item, index) => {
            index == payload.index ?( item.selected = true ) :(item.selected = false);
        })
    }
}

/*************************************************************************************************
 *                                     ASYNCHRONOUS ACTIONS
 *************************************************************************************************/
const actions = {

}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
