import Vue from 'vue'
/*************************************************************************************************
 *                                              STATE
 *************************************************************************************************/
const state = {
    config:{
        title:'AMP story builder',
        publisher:'Diwanee Serbia',
        publisherLogoSrc:'src/logo.jpg',
        posterPortraitSrc:'src/posterPortrait.jpg',
        ads:false,
    },
    slider:{
        currentIndex:0,
        count:3,
        animating:false,
    },
    pages:[]
}

/*************************************************************************************************
 *                                             GETTERS
 *************************************************************************************************/
const getters = {
    pages(state){
        //Array with story pages
        return state.pages;
    },
    selectedPage(state){
        return state.pages[state.slider.currentIndex] || false;
    },
    prevPage(state){
        if(state.slider.currentIndex - 1 >= 0) return state.pages[state.slider.currentIndex-1];
        return false;
    },
    nextPage(state){
        if(state.slider.currentIndex + 1 <= state.slider.count) return state.pages[state.slider.currentIndex+1];
        return false;
    },
    slider(state, getters){
        let temp = {...state.slider, selectedItem:getters.selectedPage.selectedItem}
        return temp;
    },
}

/*************************************************************************************************
 *                                           MUTATIONS
 *************************************************************************************************/
const mutations = {
    updateSlider(state, payload){
        state.slider = {...state.slider, ...payload};
    },
    updateBackgroundImage(state, payload ){
        state.pages[state.slider.currentIndex].background.src = `${payload.imgSrc}${payload.fullSize}`;
        state.pages[state.slider.currentIndex].background.imgId = payload.imgId;
    },
    updateCSS(state, payload){
        let temp = {...state.pages[state.slider.currentIndex][payload.type].css};
        state.pages[state.slider.currentIndex][payload.type].css = {...temp, ...payload.newValues};
    },
    updateTitle(state, newValue){
        state.pages[state.slider.currentIndex].title.text.value = newValue;
    },
    updateSelectedItem(state, newValue){
        state.pages[state.slider.currentIndex].selectedItem = newValue;
    },
    setPages(state, newValue){
        Vue.set(state, 'pages', newValue);
    },
    logMe(state){
        console.log("LOG ME");
    }
}

/*************************************************************************************************
 *                                     ASYNCHRONOUS ACTIONS
 *************************************************************************************************/
const actions = {
    setPages({commit}, newValue){
        commit("setPages", newValue)
    }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
