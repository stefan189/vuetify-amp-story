// import Vue from 'vue'
/*************************************************************************************************
 *                                              STATE
 *************************************************************************************************/
const state = {
    tools:{
        //tools icons for each TAB in app  @tabName:[ {icon1}, {icon2}... ]
        story:[
            {id:1, name:'settings', icon:'settings', selected:true, route:null },
            {id:2, name:'layout', icon:'layers', selected:false, route:null},
            {id:3, name:'background', icon:'wallpaper', selected:false, route:null},
            {id:4, name:'text', icon:'edit', selected:false, route:null},
            {id:5, name:'animation', icon:'3d_rotation', selected:false, route:null},
            {id:6, name:'bookend', icon:'class', selected:false, route:null}
        ],
        report:[
            {id:1, name:'settings', icon:'settings', selected:true, route:null },
            {id:2, name:'layout', icon:'layers', selected:false, route:null},
            {id:3, name:'background', icon:'wallpaper', selected:false, route:null},
        ]
    }
}

/*************************************************************************************************
 *                                             GETTERS
 *************************************************************************************************/
const getters = {
    selectedTools(state, getters, rootState, rootGetters){
        //Array with tools - dependet on selected tab
        return state.tools[rootGetters.selectedTab.name];
    },
    selectedTool(state, getters){
        //get selected tool obj with index prop
        return getters.selectedTools.filter((item, i)=>{
            if(item.selected==true){
                item.index = i;
                return item;
            }

        })[0];
    },
}

/*************************************************************************************************
 *                                           MUTATIONS
 *************************************************************************************************/
const mutations = {
    updateSelectedTool(state, payload){
        state.tools[payload.selectedTabName].map((item, index) => {
            index == payload.index ?( item.selected = true ) :(item.selected = false);
        })
    }
}

/*************************************************************************************************
 *                                     ASYNCHRONOUS ACTIONS
 *************************************************************************************************/
const actions = {

}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
