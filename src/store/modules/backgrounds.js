// import Vue from 'vue'
/*************************************************************************************************
 *                                              STATE
 *************************************************************************************************/
const state = {
    backgrounds:[
        {imgSrc:'https://picsum.photos', thumb:'/360/640', fullSize:'/720/1280', imgId:'?image=10', name:'image 10', selected:true},
        {imgSrc:'https://picsum.photos', thumb:'/360/640', fullSize:'/720/1280', imgId:'?image=11', name:'image 11', selected:false},
        {imgSrc:'https://picsum.photos', thumb:'/360/640', fullSize:'/720/1280', imgId:'?image=12', name:'image 12', selected:false},
        {imgSrc:'https://picsum.photos', thumb:'/360/640', fullSize:'/720/1280', imgId:'?image=13', name:'image 13', selected:false},
        {imgSrc:'https://picsum.photos', thumb:'/360/640', fullSize:'/720/1280', imgId:'?image=14', name:'image 14', selected:false},
        {imgSrc:'https://picsum.photos', thumb:'/360/640', fullSize:'/720/1280', imgId:'?image=15', name:'image 15', selected:false},
        {imgSrc:'https://picsum.photos', thumb:'/360/640', fullSize:'/720/1280', imgId:'?image=1071', name:'image 1071', selected:false},
        {imgSrc:'https://picsum.photos', thumb:'/360/640', fullSize:'/720/1280', imgId:'?image=1072', name:'image 1072', selected:false},
        {imgSrc:'https://picsum.photos', thumb:'/360/640', fullSize:'/720/1280', imgId:'?image=1073', name:'image 1073', selected:false},
        {imgSrc:'https://picsum.photos', thumb:'/360/640', fullSize:'/720/1280', imgId:'?image=1074', name:'image 1074', selected:false},
        {imgSrc:'https://picsum.photos', thumb:'/360/640', fullSize:'/720/1280', imgId:'?image=1075', name:'image 1075', selected:false},
        {imgSrc:'https://picsum.photos', thumb:'/360/640', fullSize:'/720/1280', imgId:'?image=1076', name:'image 1076', selected:false},
    ]
}

/*************************************************************************************************
 *                                             GETTERS
 *************************************************************************************************/
const getters = {
    backgrounds(state){
        //Array with backgrounds
        return state.backgrounds;
    },
    selectedBackground(state){
        //get selected Background Obj wiht index prop
        return state.backgrounds.filter((item, i)=>{
            if(item.selected==true){
                item.index = i;
                return item;
            }

        })[0];
    }
}

/*************************************************************************************************
 *                                           MUTATIONS
 *************************************************************************************************/
const mutations = {
    updateSelectedBackground(state, payload){
        state.backgrounds.map((item) => {
            item.imgId == payload.imgId ?( item.selected = true ) :(item.selected = false);
        })
    }
}

/*************************************************************************************************
 *                                     ASYNCHRONOUS ACTIONS
 *************************************************************************************************/
const actions = {

}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
