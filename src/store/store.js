import Vue from 'vue'
import Vuex from 'vuex'

/*************************************************************************************************
 *                                             MODULES IMPORT
 *************************************************************************************************/
import tabs from "@/store/modules/tabs"
import tools from "@/store/modules/tools"
import story from "@/store/modules/story"
import backgrounds from "@/store/modules/backgrounds"
// import editors from "@/store/modules/editors"

Vue.use(Vuex)

export default new Vuex.Store({
    ...tabs,//Root State
    modules: {tools, backgrounds, story}//Modules
})
